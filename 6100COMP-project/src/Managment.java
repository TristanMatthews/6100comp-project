
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import java.math.BigInteger;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Managment {

	static Scanner scan = new Scanner(System.in);
	static SystemUser user;
	static ArrayList<Employee> workers = new ArrayList<Employee>();
	static ArrayList<Employee> workersOnSite = new ArrayList<Employee>();
	static Map<String, Materials> materialsList = new HashMap<String, Materials>();
	static Job job;
	static Client client;
	static String notAdmin = "\nSorry you must be admin to do this\n";
	static Materials onSiteMaterials;
	static Connect c = new Connect();
	static Connection conn = c.makeConnnection();

	public static void main(String[] args) {

		boolean flag = false;

		if (loggin()) {
			String choice;
			allWorkers();
			while (flag != true) {

				System.out.println("---- Main Menu ---- \n 1. Worker \n 2. Job \n 3. Calculations \n Q. Quit");
				choice = scan.nextLine();

				if (choice.matches("[1-3]|[qQ]")) {
					switch (choice) {
					case "1":
						workerChoice();
						break;
					case "2":
						jobChoice();
						break;
					case "3":
						calcChoice();
						break;

					case "Q":
					case "q":
						System.exit(0);

					}
				} else {
					System.out.println("\nPlease input one of the options\n");
				}
			}
		} else {
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.exit(0);

		}

	}

	public static boolean loggin() {
		boolean loggedin = false;
		String username;
		String password;
		String id = "";
		while (!loggedin) {
			System.out.println("Please enter your username");
			username = scan.nextLine();
			System.out.println("Please enter your password");
			password = scan.nextLine();

			// hash the password using md5
			String hashPass = hashPass(password);

			String sql = "Select workerid from users where username=? and password=?";
			try {

				PreparedStatement stmt = conn.prepareStatement(sql);
				stmt.setString(1, username);
				stmt.setString(2, hashPass);

				ResultSet rs = stmt.executeQuery();
				if (!rs.isBeforeFirst()) {
					System.out.println("Some of your credentials are incorrect\n----");
				} else {

					while (rs.next()) {
						id = rs.getString("WorkerID");
					}
					sql = "select * from workers where workerid =?";
					stmt = conn.prepareStatement(sql);
					stmt.setString(1, id);
					rs = stmt.executeQuery();

					while (rs.next()) {
						user = new SystemUser(rs.getString("FirstName"), rs.getString("SecondName"), id,
								rs.getString("JobID"), rs.getDouble("Wage"), rs.getString("Occupation"), username,
								hashPass);

					}
					getJob(user.getJobRef());
					loggedin = true;
				}

			} catch (SQLException ex) {
				sqlExceptionError(ex);
			}
		}
		return loggedin;
	}

	public static void getJob(String jobRef) {
		Materials materials;
		String clientID = "";


		String sql = ("Select * from materials where jobID = ?");

		try {
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setString(1, jobRef);
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				materials = new Materials(rs.getString("MaterialsID"), rs.getString("jobID"), rs.getInt("Blocks"),
						rs.getInt("Bricks"), rs.getFloat("Concrete"), rs.getFloat("StuddingTimber"),
						rs.getFloat("PlasterBoard"), rs.getInt("RafterAmount"), rs.getFloat("RafterLength"),
						rs.getInt("RoofCover"), rs.getFloat("SkirtingBoard"), rs.getFloat("Archatrave"),
						rs.getFloat("Guttering"), rs.getInt("Doors"), rs.getInt("Windows"), rs.getInt("Hinges"),
						rs.getInt("Handles"), rs.getInt("Locks"), rs.getString("Type"));
				materialsList.put(materials.getType(), materials);
				if (rs.getString("Type").equalsIgnoreCase("onSite")) {
					onSiteMaterials = new Materials(rs.getString("MaterialsID"), rs.getString("jobID"),
							rs.getInt("Blocks"), rs.getInt("Bricks"), rs.getFloat("Concrete"),
							rs.getFloat("StuddingTimber"), rs.getFloat("PlasterBoard"), rs.getInt("RafterAmount"),
							rs.getFloat("RafterLength"), rs.getInt("RoofCover"), rs.getFloat("SkirtingBoard"),
							rs.getFloat("Archatrave"), rs.getFloat("Guttering"), rs.getInt("Doors"),
							rs.getInt("Windows"), rs.getInt("Hinges"), rs.getInt("Handles"), rs.getInt("Locks"),
							rs.getString("Type"));
				}
			}

			sql = ("Select * from jobs where jobID = ?");
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, jobRef);
			rs = stmt.executeQuery();
			while (rs.next()) {
				job = new Job(rs.getDate("Deadline"), rs.getDouble("Price"), rs.getString("JobID"), materialsList,
						workersOnSite, rs.getString("AddressID"));
				clientID = rs.getString("ClientID");
			}

			sql = ("Select * from clients where ClientID = ?");
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, clientID);
			rs = stmt.executeQuery();
			while (rs.next()) {
				client = new Client(rs.getString("FirstName"), rs.getString("SecondName"), rs.getString("ClientID"),
						rs.getString("PhoneNumber"), rs.getString("EmailAddress"));

			}
		} catch (SQLException ex) {
			sqlExceptionError(ex);
		}

	}

	public static void workerChoice() {

		String choice;
		boolean flag = false;

		while (!flag) {
			System.out.println(
					"---- Worker Menu ---- \n 1. View Worker \n 2. Edit Worker \n 3. Create Worker \n 4. Move Worker \n 5. Delete Worker \n 6. View workers on site \n 7. View all workers \n Q. Quit \n B. Back");
			choice = scan.nextLine();

			if (choice.matches("[1-7]|[bB|qQ]")) {
				switch (choice) {
				case "1":
					user.viewWorker();
					break;
				case "2":
					if (user.isAdmin()) {
						user.editWorker();
					} else {
						System.out.println(notAdmin);
					}
					break;
				case "3":
					if (user.isAdmin()) {
						user.createWorker();
					} else {
						System.out.println(notAdmin);
					}
					break;
				case "4":
					if (user.isAdmin()) {
						user.moveWorker();
					} else {
						System.out.println(notAdmin);
					}
					break;
				case "5":
					if (user.isAdmin()) {
						user.deleteWorker();
					} else {
						System.out.println(notAdmin);
					}
					break;
				case "6":
					if (user.getJobRef() != null) {
						printWorkers("onSite");
					} else {
						System.out.println("\nYou are not assigned to a site\n");
					}
					break;
				case "7":
					if (user.isAdmin()) {
						printWorkers("allWorkers");
					} else {
						System.out.println(notAdmin);
					}
					break;
				case "Q":
				case "q":
					try {
						conn.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					System.exit(0);
				case "B":
				case "b":
					return;
					default:
						workerChoice();
				}
			} else {
				System.out.println("\nPlease input one of the options\n");
			}
		}
	}

	public static void jobChoice() {

		String choice;
		boolean flag = false;
		while (!flag) {
			System.out.println(
					"---- Job Menu ---- \n 1. View Job \n 2. View Required Materials \n 3. View Materials on Site \n 4. Make progress report \n 5. View all jobs \n 6. Update materials on site \n 7. Create job \n 8. Delete job \n Q. Quit \n B. Back");
			choice = scan.nextLine();

			if (choice.matches("[1-8]|[bB|qQ]")) {
				switch (choice) {
				case "1":
					if(job != null) 
					job.viewJob();
					else
					System.out.println("\nYou are not assigned to a job\n");
						
					break;
				case "2":
					if(job != null)
					job.printMaterials("required");
					else
						System.out.println("\nYou are not assigned to a job\n");
					break;
				case "3":
					if(job != null)
					job.printMaterials("onSite");
					else
						System.out.println("\nYou are not assigned to a job\n");
					break;
				case "4":
					user.progressReport();
					break;
				case "5":
					if (user.isAdmin()) {
						viewAllJobs();
					} else {
						System.out.println(notAdmin);
					}
					break;
				case "6":
					user.updateMaterials();
					break;
				case "7":
					if (user.isAdmin()) {
						user.createClient();
					} else {
						System.out.println(notAdmin);
					}
					break;
				case "8":
					if (user.isAdmin()) {
						user.deleteJob();
					} else {
						System.out.println(notAdmin);
					}
					break;
				case "Q":
				case "q":
					try {
						conn.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					System.exit(0);
				case "B":
				case "b":
					return;
				}
			} else {
				System.out.println("\nPlease Input one of the options\n");
			}
		}
	}

	public static void calcChoice() {
		String choice;
		boolean flag = false;
		while (!flag) {
			System.out.println(
					"---- Job Menu ---- \n 1. Materials per meter of walls  \n Q. Quit \n B. Back");
			choice = scan.nextLine();

			if (choice.matches("[1-5]|[bB|qQ]")) {
				switch (choice) {
				case "1":
					wallMaterials();
					break;
				case "2":

					break;
				case "3":

					break;
				case "4":

					break;
				case "Q":
				case "q":
					try {
						conn.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					System.exit(0);
				case "B":
				case "b":
					return;
				}
			} else {
				System.out.println("\nPlease Input one of the options\n");
			}
		}
	}
	
	
	
	
	public static void viewAllJobs() {
		String sql = ("select * from jobs,clients, addresses where jobs.ClientID=clients.ClientID and jobs.AddressId=addresses.addressid");
		try {
			System.out.println("\n--- All Jobs ---\n");
			PreparedStatement stmt = conn.prepareStatement(sql);

			ResultSet rs = stmt.executeQuery();

			if (!rs.isBeforeFirst()) {
				return;
			} else {
				while (rs.next()) {
					Job theJob = new Job(rs.getDate("Deadline"), rs.getDouble("Price"), rs.getString("JobID"), null,
							null, rs.getString("AddressID"));
					System.out.println(
							"----\nJob Reference: " + theJob.getJobRef() + "\nDeadLine: " + theJob.getDate());
					
					System.out.println("\nclient - \nName: " + rs.getString("FirstName") + " "
							+ rs.getString("SecondName") + "\nPhone number: " + rs.getString("PhoneNumber")
							+ "\nEmail address: " + rs.getString("EmailAddress"));

					theJob.getAddress();

				}
			}
		} catch (SQLException ex) {
			// handle any errors
			sqlExceptionError(ex);
		}
	}

	public static void allWorkers() {

		workers.clear();
		workersOnSite.clear();
		String sql = ("select * from workers");
		try {

			PreparedStatement stmt = conn.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();

			if (!rs.isBeforeFirst()) {
				return;
			} else {
				while (rs.next()) {
					Employee employee = new Employee(rs.getString("FirstName"), rs.getString("SecondName"),
							rs.getString("WorkerID"), rs.getString("JobID"), rs.getDouble("Wage"),
							rs.getString("Occupation"));
					workers.add(employee);
					if (rs.getString("JobID") != null && rs.getString("JobID").equalsIgnoreCase(user.getJobRef())) {
						workersOnSite.add(employee);
					}
				}
			}

		} catch (SQLException ex) {
			// handle any errors
			sqlExceptionError(ex);
		}

	}

	public static void printWorkers(String whichWorkers) {

		allWorkers();
		if (whichWorkers.equalsIgnoreCase("allWorkers")) {
			for (Employee worker : workers) {
				System.out.println("\nWorker ID: " + worker.getId() + "\nFirst name: " + worker.getFirstName()
						+ "\nSecond name: " + worker.getSecondName() + "\nOccupation: " + worker.getOccupation()
						+ "\nJob reference: " + worker.getJobRef() + "\n");
			}
		} else if (whichWorkers.equalsIgnoreCase("onSite")) {

			for (Employee worker : workersOnSite) {
				if (worker.getJobRef().equals(user.getJobRef())) {
					System.out.println("\nWorker ID: " + worker.getId() + "\nFirst name: " + worker.getFirstName()
							+ "\nSecond name: " + worker.getSecondName() + "\nOccupation: " + worker.getOccupation()
							+ "\nJob reference: " + worker.getJobRef() + "\n");
				}
			}
		}
	}

	public static void wallMaterials() {
		int meter = 1000;
		String metersOfWall;
		String height;
		int studs;
		int studDistance = 400;
		double plasterboardSize = 2.88;
		double plasterboardNeeded;

		System.out.println("How many meters of wall are there?");
		metersOfWall = scan.nextLine();
		System.out.println("How high is the wall?");
		height = scan.nextLine();

		studs = (Integer.parseInt(metersOfWall) * meter) / studDistance;
		plasterboardNeeded = ((Integer.parseInt(height) * Integer.parseInt(metersOfWall)) / plasterboardSize);

		System.out.println(
				"Studs required: " + studs + "\n" + "Plasterboard required: " + Math.round(plasterboardNeeded) + "\n");

	}
	public static boolean isExists(String sql, String value) {
		try {

			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setString(1, value);

			ResultSet rs = stmt.executeQuery();

			if (!rs.isBeforeFirst()) {
				return false;
			}
		} catch (SQLException ex) {
			// handle any errors
			sqlExceptionError(ex);
		}
		return true;
	}

	public static String hashPass(String pass) {

		String passHash = "";
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");

			byte[] message = md.digest(pass.getBytes(Charset.forName("UTF-8")));

			BigInteger num = new BigInteger(1, message);

			passHash = num.toString(16);
			while (passHash.length() < 32) {
				passHash = "" + passHash;
			}

		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return passHash;

	}

	public static void sqlExceptionError(SQLException ex) {
		System.out.println("SQLException: " + ex.getMessage());
		System.out.println("SQLState: " + ex.getSQLState());
		System.out.println("VendorError: " + ex.getErrorCode());
	}
}
