
public class Client extends User{

	private String phoneNumber;
	private String emailAddress;
	
	public Client(String firstName, String secondName, String id, String phoneNumber, String emailAddress) {
		super(firstName, secondName, id);
		this.phoneNumber = phoneNumber;
		this.emailAddress = emailAddress;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	
}
