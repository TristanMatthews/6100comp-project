
abstract class User {
	
	protected String firstName;
	protected String secondName;
	protected String id;
		
	protected User(String firstName, String secondName, String id) {
		
		this.firstName = firstName;
		this.secondName = secondName;
		this.id = id;
	}
	
	protected String getId() {
		return id;
	}
	protected void setId(String id) {
		this.id = id;
	}
	
	protected String getFirstName() {
		return firstName;
	}
	protected void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	protected String getSecondName() {
		return secondName;
	}
	protected void setSecondName(String secondName) {
		this.secondName = secondName;
	}
	
}
