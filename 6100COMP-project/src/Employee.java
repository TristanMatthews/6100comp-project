
public class Employee extends User{
	
	protected String jobRef;
	protected double wage;
	protected String occupation;
	
	public Employee(String firstName, String secondName, String id, String jobRef, double wage, String occupation) {
		super(firstName,secondName,id);
		this.jobRef = jobRef;
		this.wage = wage;
		this.occupation = occupation;
	}
	

	public String getJobRef() {
		return jobRef;
	}


	public void setJobRef(String jobRef) {
		this.jobRef = jobRef;
	}


	public double getWage() {
		return wage;
	}

	public void setWage(double wage) {
		this.wage = wage;
	}

	public String getOccupation() {
		return occupation;
	}

	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}
	
	public double calcWage(double hoursworked) {
		double owed = wage * hoursworked;
		return owed;
	}

}
