import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Connect {
	
	private String databaseName = "project_database";
	private String username = "root";
	private String password = "";
	

	public Connect() {
		// TODO Auto-generated constructor stub
	}
	
	public Connection makeConnnection() {
		Connection conn = null;
		try {

			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/" + databaseName, username, password);

		} catch (SQLException ex) {
			// handle any errors
			System.out.println(ex);
		}
		return conn;
	}

}
