
public class Materials {

	private String id;
	private String jobRef;
	private int blocks;
	private int bricks;
	private float concrete;
	private float studdingTimber;
	private float plasterBoard;
	private int rafterAmount;
	private float rafterLength;
	private int roofCover;
	private float skirtingBoard;
	private float archatrave;
	private float guttering;
	private int doors;
	private int windows;
	private int hinges;
	private int handles;
	private int locks;
	private String Type;
	
	

	public Materials(String id, String jobRef, int blocks, int bricks, float concrete, float studdingTimber,
			float plasterBoard, int rafterAmount, float rafterLength, int roofCover, float skirtingBoard,
			float archatrave, float guttering, int doors, int windows, int hinges, int handles, int locks,
			String type) {
		super();
		this.id = id;
		this.jobRef = jobRef;
		this.blocks = blocks;
		this.bricks = bricks;
		this.concrete = concrete;
		this.studdingTimber = studdingTimber;
		this.plasterBoard = plasterBoard;
		this.rafterAmount = rafterAmount;
		this.rafterLength = rafterLength;
		this.roofCover = roofCover;
		this.skirtingBoard = skirtingBoard;
		this.archatrave = archatrave;
		this.guttering = guttering;
		this.doors = doors;
		this.windows = windows;
		this.hinges = hinges;
		this.handles = handles;
		this.locks = locks;
		Type = type;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getJobRef() {
		return jobRef;
	}

	public void setJobRef(String jobRef) {
		this.jobRef = jobRef;
	}

	public int getBlocks() {
		return blocks;
	}

	public void setBlocks(int blocks) {
		this.blocks = blocks;
	}

	public int getBricks() {
		return bricks;
	}

	public void setBricks(int bricks) {
		this.bricks = bricks;
	}

	public float getConcrete() {
		return concrete;
	}

	public void setConcrete(float concrete) {
		this.concrete = concrete;
	}

	public float getStuddingTimber() {
		return studdingTimber;
	}

	public void setStuddingTimber(float studdingTimber) {
		this.studdingTimber = studdingTimber;
	}

	public float getPlasterBoard() {
		return plasterBoard;
	}

	public void setPlasterBoard(float plasterBoard) {
		this.plasterBoard = plasterBoard;
	}

	public int getRafterAmount() {
		return rafterAmount;
	}

	public void setRafterAmount(int rafterAmount) {
		this.rafterAmount = rafterAmount;
	}

	public float getRafterLength() {
		return rafterLength;
	}

	public void setRafterLength(float rafterLength) {
		this.rafterLength = rafterLength;
	}

	public int getRoofCover() {
		return roofCover;
	}

	public void setRoofCover(int roofCover) {
		this.roofCover = roofCover;
	}

	public float getSkirtingBoard() {
		return skirtingBoard;
	}

	public void setSkirtingBoard(float skirtingBoard) {
		this.skirtingBoard = skirtingBoard;
	}

	public float getArchatrave() {
		return archatrave;
	}

	public void setArchatrave(float archatrave) {
		this.archatrave = archatrave;
	}

	public float getGuttering() {
		return guttering;
	}

	public void setGuttering(float guttering) {
		this.guttering = guttering;
	}

	public int getDoors() {
		return doors;
	}

	public void setDoors(int doors) {
		this.doors = doors;
	}

	public int getWindows() {
		return windows;
	}

	public void setWindows(int windows) {
		this.windows = windows;
	}

	public int getHinges() {
		return hinges;
	}

	public void setHinges(int hinges) {
		this.hinges = hinges;
	}

	public int getHandles() {
		return handles;
	}

	public void setHandles(int handles) {
		this.handles = handles;
	}

	public int getLocks() {
		return locks;
	}

	public void setLocks(int locks) {
		this.locks = locks;
	}

	public String getType() {
		return Type;
	}

	public void setType(String type) {
		Type = type;
	}

	
}
