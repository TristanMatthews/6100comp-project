import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Savepoint;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.format.ResolverStyle;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Map;
import java.util.Scanner;

public class SystemUser extends Employee {

	Scanner scan = new Scanner(System.in);
	private String username;
	private String password;

	public SystemUser(String firstName, String secondName, String id, String jobRef, double wage, String occupation,
			String username, String password) {
		super(firstName, secondName, id, jobRef, wage, occupation);
		this.username = username;
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isAdmin() {
		boolean check = false;
		if (this.getOccupation().equalsIgnoreCase("admin")) {
			check = true;
		}
		return check;
	}

	public void createClient() {
		System.out.println("Are you creating a job for an already existing client? (Y/N)");
		String yesNo = scan.nextLine();
		boolean check = false;
		String existing = "";
		if (yesNo.equalsIgnoreCase("y") || yesNo.equalsIgnoreCase("yes")) {
			existing = "yes";
			while (!check) {
				System.out.println("What is the Clients Email address?");
				String email = scan.nextLine();

				if (email.matches(
						"(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])")) {
					if (Managment.isExists("select clientID from clients where emailaddress=?", email)) {
						check = true;
						Client client = new Client(null, null, null, null, email);
						Map<String, String> address = createAddress();
						if (!address.isEmpty()) {
							createJob(client, address, existing);
						} else {
							System.out.println("\nPlease reenter address information\n");
							createAddress();
						}
					} else {
						System.out.println("\nThis client email doesnt exist\n");
					}
				}
			}
		} else if (yesNo.equalsIgnoreCase("n") || yesNo.equalsIgnoreCase("no")) {
			existing = "no";
			while (!check) {

				System.out.println("What is the clients first name?");
				String clientFirstName = scan.nextLine();
				System.out.println("What is the clients second name?");
				String clientSecondName = scan.nextLine();
				System.out.println("What is the clients email address?");
				String clientEmail = scan.nextLine();
				System.out.println("What is the clients phone number?(Eleven digits no spaces)");
				String clientNumber = scan.nextLine();

				if (clientFirstName.matches("^[aA-zZ]+$") && clientSecondName.matches("^[aA-zZ]+$")
						&& clientEmail.matches(
								"(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])")
						&& clientNumber.matches("^[0-9]{11}$")) {

					if (!Managment.isExists("select clientID from clients where emailaddress=?", clientEmail)) {
						// upper case the first letter of names
						String firstNameUpper = clientFirstName.substring(0, 1).toUpperCase()
								+ clientFirstName.substring(1).toLowerCase();
						String secondNameUpper = clientSecondName.substring(0, 1).toUpperCase()
								+ clientSecondName.substring(1).toLowerCase();

						check = true;
						Client client = new Client(firstNameUpper, secondNameUpper, null, clientNumber, clientEmail);

						Map<String, String> address = createAddress();
						if (!address.isEmpty()) {
							createJob(client, address, existing);

						} else {
							System.out.println("\nPlease reenter address information\n");
							createAddress();
						}
					} else {
						System.out.println("\nThis client email already exists\n");
					}

				} else {
					System.out.println(
							"Please input these credentials in appropriate format (Phone number eleven digits no spaces)");
				}
			}
		}
	}

	public Map<String, String> createAddress() {
		boolean check = false;
		Map<String, String> address = new HashMap<String, String>();
		while (!check) {
			System.out.println("\nPlease enter the house number of the job");
			String houseNum = scan.nextLine();
			System.out.println("Please enter the first line of the address");
			String lineOne = scan.nextLine();
			System.out.println("Please enter the second line if there is one (If no second line just press enter)");
			String lineTwo = scan.nextLine();
			System.out.println("Please enter the postcode");
			String postcode = scan.nextLine();
			System.out.println("Please enter the county");
			String county = scan.nextLine();
			System.out.println("Please enter the city");
			String city = scan.nextLine();

			if (houseNum.matches("^[1-9]+$") && lineOne.matches("^(?:([aA-zZ]+) ?[aA-zZ]*)$")
					&& lineTwo.matches("^$|(?:([aA-zZ]+) ?[aA-zZ]*)$")
					&& postcode.matches("^[a-z]{1,2}\\d[a-z\\d]?\\s*\\d[a-z]{2}$")
					&& county.matches("^(?:([aA-zZ]+) ?[aA-zZ]*)$") && city.matches("^(?:([aA-zZ]+) ?[aA-zZ]*)$")) {
				check = true;
				String countyUpper = county.substring(0, 1).toUpperCase() + county.substring(1).toLowerCase();
				String cityUpper = city.substring(0, 1).toUpperCase() + city.substring(1).toLowerCase();

				address.put("houseNumber", houseNum);
				address.put("lineOne", lineOne);
				if (!lineTwo.isEmpty()) {
					address.put("lineTwo", lineTwo);
				} else
					address.put("lineTwo", null);
				address.put("postcode", postcode);
				address.put("county", countyUpper);
				address.put("city", cityUpper);
			} else {
				System.out.println("Please input these credentials in appropriate format");
			}

		}
		return address;
	}

	public void createJob(Client client, Map<String, String> address, String existing) {
		// make the connection to the database using connect class
		Connect c = new Connect();
		Connection conn = c.makeConnnection();
		boolean check = false;
		while (!check) {
			System.out.println("Please enter the job reference you want for this job");
			String jobRef = scan.nextLine();
			System.out.println("Please enter the price of the job");
			String price = scan.nextLine();
			System.out.println("Please enter the finish date of the job (Format YYYY-MM-DD)");
			String date = scan.nextLine();

			try {
				LocalDate.parse(date, DateTimeFormatter.ofPattern("uuuu-M-d").withResolverStyle(ResolverStyle.STRICT));
			} catch (DateTimeParseException e) {
				System.out.println("\nThere was something wrong with the date entered please try again\n");
				continue;
			}

			if (jobRef.matches("^([aA-zZ]|\\d)+$") && price.matches("\\d+.?\\d*")) {
				check = true;
				Materials requiredMaterials = createMaterials(jobRef, "required");
				Materials onSiteMaterials = createMaterials(jobRef, "onsite");
				if (existing.equalsIgnoreCase("yes")) {
					try {
						// prevents sql statments from running immediately, wait for commit
						conn.setAutoCommit(false);

						Savepoint save = conn.setSavepoint("save");

						String sql = "insert into addresses(LineOne,LineTwo,PostCode,HouseNumber,County,City)values(?,?,?,?,?,?);";
						PreparedStatement stmt = conn.prepareStatement(sql);
						stmt.setString(1, address.get("lineOne"));
						stmt.setString(2, address.get("lineTwo"));
						stmt.setString(3, address.get("postcode"));
						stmt.setString(4, address.get("houseNumber"));
						stmt.setString(5, address.get("county"));
						stmt.setString(6, address.get("city"));

						int count = stmt.executeUpdate();
						if (count > 0) {
							System.out.println("\n--- Address created ---\n");
							count = 0;

							sql = "insert into jobs(JobID,AddressID,ClientID,Deadline,Price)values(?,(select LAST_INSERT_ID()),(select ClientID from clients where EmailAddress=?),?,?);";
							stmt = conn.prepareStatement(sql);
							stmt.setString(1, jobRef);
							stmt.setString(2, client.getEmailAddress());
							stmt.setString(3, date);
							stmt.setString(4, price);
							count = stmt.executeUpdate();
							if (count > 0) {
								System.out.println("\n--- Job created ---\n");
								count = 0;
								sql = "insert into materials(JobID,Blocks, Bricks, Concrete, StuddingTimber, PlasterBoard, RafterAmount, RafterLength, RoofCover, SkirtingBoard, Archatrave, Guttering, Doors, Windows, Hinges, Handles, Locks, Type)"
										+ "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";

								stmt = conn.prepareStatement(sql);
								stmt.setString(1, requiredMaterials.getJobRef());
								stmt.setInt(2, requiredMaterials.getBlocks());
								stmt.setInt(3, requiredMaterials.getBricks());
								stmt.setFloat(4, requiredMaterials.getConcrete());
								stmt.setFloat(5, requiredMaterials.getStuddingTimber());
								stmt.setFloat(6, requiredMaterials.getPlasterBoard());
								stmt.setInt(7, requiredMaterials.getRafterAmount());
								stmt.setFloat(8, requiredMaterials.getRafterLength());
								stmt.setInt(9, requiredMaterials.getRoofCover());
								stmt.setFloat(10, requiredMaterials.getSkirtingBoard());
								stmt.setFloat(11, requiredMaterials.getArchatrave());
								stmt.setFloat(12, requiredMaterials.getGuttering());
								stmt.setInt(13, requiredMaterials.getDoors());
								stmt.setInt(14, requiredMaterials.getWindows());
								stmt.setInt(15, requiredMaterials.getHinges());
								stmt.setInt(16, requiredMaterials.getHandles());
								stmt.setInt(17, requiredMaterials.getLocks());
								stmt.setString(18, requiredMaterials.getType());

								count = stmt.executeUpdate();
								if (count > 0) {
									count = 0;
									System.out.println("\n--- Materials required created ---\n");

									sql = "insert into materials(JobID,Blocks, Bricks, Concrete, StuddingTimber, PlasterBoard, RafterAmount, RafterLength, RoofCover, SkirtingBoard, Archatrave, Guttering, Doors, Windows, Hinges, Handles, Locks, Type)"
											+ "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
									stmt = conn.prepareStatement(sql);
									stmt.setString(1, onSiteMaterials.getJobRef());
									stmt.setInt(2, onSiteMaterials.getBlocks());
									stmt.setInt(3, onSiteMaterials.getBricks());
									stmt.setFloat(4, onSiteMaterials.getConcrete());
									stmt.setFloat(5, onSiteMaterials.getStuddingTimber());
									stmt.setFloat(6, onSiteMaterials.getPlasterBoard());
									stmt.setInt(7, onSiteMaterials.getRafterAmount());
									stmt.setFloat(8, onSiteMaterials.getRafterLength());
									stmt.setInt(9, onSiteMaterials.getRoofCover());
									stmt.setFloat(10, onSiteMaterials.getSkirtingBoard());
									stmt.setFloat(11, onSiteMaterials.getArchatrave());
									stmt.setFloat(12, onSiteMaterials.getGuttering());
									stmt.setInt(13, onSiteMaterials.getDoors());
									stmt.setInt(14, onSiteMaterials.getWindows());
									stmt.setInt(15, onSiteMaterials.getHinges());
									stmt.setInt(16, onSiteMaterials.getHandles());
									stmt.setInt(17, onSiteMaterials.getLocks());
									stmt.setString(18, onSiteMaterials.getType());

									count = stmt.executeUpdate();
									if (count > 0) {
										System.out.println("\n--- Materials on site created ---\n");
										conn.commit();
									} else {
										System.out.println(
												"\nMaterials on site couldnt be created, nothing pushed to database\n");
										conn.rollback(save);
									}
								} else {
									System.out.println("\nMaterials couldnt be created");
									conn.rollback(save);
								}
							} else {
								System.out.println("\nJob couldnt be created");
								conn.rollback(save);
							}
						} else {
							System.out.println("\nAddress couldnt be created");
							conn.rollback(save);
						}

					} catch (SQLException ex) {
						// handle any errors
						Managment.sqlExceptionError(ex);

					}
				} else if (existing.equalsIgnoreCase("no")) {
					try {
						conn.setAutoCommit(false);
						Savepoint save = conn.setSavepoint("save");
						String sql = "insert into clients(EmailAddress,FirstName,SecondName,PhoneNumber)values(?,?,?,?);";

						PreparedStatement stmt = conn.prepareStatement(sql);
						stmt.setString(1, client.getEmailAddress());
						stmt.setString(2, client.getFirstName());
						stmt.setString(3, client.getSecondName());
						stmt.setString(4, client.getPhoneNumber());

						int count = stmt.executeUpdate();
						if (count > 0) {
							count = 0;
							System.out.println("\n-- Client created -- \n");

							sql = "insert into addresses(LineOne,LineTwo,PostCode,HouseNumber,County,City)values(?,?,?,?,?,?);";

							stmt = conn.prepareStatement(sql);
							stmt.setString(1, address.get("lineOne"));
							stmt.setString(2, address.get("lineTwo"));
							stmt.setString(3, address.get("postcode"));
							stmt.setString(4, address.get("houseNumber"));
							stmt.setString(5, address.get("county"));
							stmt.setString(6, address.get("city"));

							count = stmt.executeUpdate();
							if (count > 0) {
								count = 0;
								System.out.println("\n--- Address created ---\n");

								sql = "insert into jobs(JobID,AddressID,ClientID,Deadline,Price)values(?,(select LAST_INSERT_ID()),(select clientid from clients where emailaddress=?),?,?);";
								stmt = conn.prepareStatement(sql);
								stmt.setString(1, jobRef);
								stmt.setString(2, client.getEmailAddress());
								stmt.setString(3, date);
								stmt.setString(4, price);

								count = stmt.executeUpdate();
								if (count > 0) {
									System.out.println("\n--- Job created ---\n");
									count = 0;
									sql = "insert into materials(JobID,Blocks, Bricks, Concrete, StuddingTimber, PlasterBoard, RafterAmount, RafterLength, RoofCover, SkirtingBoard, Archatrave, Guttering, Doors, Windows, Hinges, Handles, Locks, Type)"
											+ "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";

									stmt = conn.prepareStatement(sql);
									stmt.setString(1, requiredMaterials.getJobRef());
									stmt.setInt(2, requiredMaterials.getBlocks());
									stmt.setInt(3, requiredMaterials.getBricks());
									stmt.setFloat(4, requiredMaterials.getConcrete());
									stmt.setFloat(5, requiredMaterials.getStuddingTimber());
									stmt.setFloat(6, requiredMaterials.getPlasterBoard());
									stmt.setInt(7, requiredMaterials.getRafterAmount());
									stmt.setFloat(8, requiredMaterials.getRafterLength());
									stmt.setInt(9, requiredMaterials.getRoofCover());
									stmt.setFloat(10, requiredMaterials.getSkirtingBoard());
									stmt.setFloat(11, requiredMaterials.getArchatrave());
									stmt.setFloat(12, requiredMaterials.getGuttering());
									stmt.setInt(13, requiredMaterials.getDoors());
									stmt.setInt(14, requiredMaterials.getWindows());
									stmt.setInt(15, requiredMaterials.getHinges());
									stmt.setInt(16, requiredMaterials.getHandles());
									stmt.setInt(17, requiredMaterials.getLocks());
									stmt.setString(18, requiredMaterials.getType());

									count = stmt.executeUpdate();
									if (count > 0) {
										count = 0;
										System.out.println("\n--- Materials required created ---\n");

										sql = "insert into materials(JobID,Blocks, Bricks, Concrete, StuddingTimber, PlasterBoard, RafterAmount, RafterLength, RoofCover, SkirtingBoard, Archatrave, Guttering, Doors, Windows, Hinges, Handles, Locks, Type)"
												+ "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
										stmt = conn.prepareStatement(sql);
										stmt.setString(1, onSiteMaterials.getJobRef());
										stmt.setInt(2, onSiteMaterials.getBlocks());
										stmt.setInt(3, onSiteMaterials.getBricks());
										stmt.setFloat(4, onSiteMaterials.getConcrete());
										stmt.setFloat(5, onSiteMaterials.getStuddingTimber());
										stmt.setFloat(6, onSiteMaterials.getPlasterBoard());
										stmt.setInt(7, onSiteMaterials.getRafterAmount());
										stmt.setFloat(8, onSiteMaterials.getRafterLength());
										stmt.setInt(9, onSiteMaterials.getRoofCover());
										stmt.setFloat(10, onSiteMaterials.getSkirtingBoard());
										stmt.setFloat(11, onSiteMaterials.getArchatrave());
										stmt.setFloat(12, onSiteMaterials.getGuttering());
										stmt.setInt(13, onSiteMaterials.getDoors());
										stmt.setInt(14, onSiteMaterials.getWindows());
										stmt.setInt(15, onSiteMaterials.getHinges());
										stmt.setInt(16, onSiteMaterials.getHandles());
										stmt.setInt(17, onSiteMaterials.getLocks());
										stmt.setString(18, onSiteMaterials.getType());

										count = stmt.executeUpdate();
										if (count > 0) {
											System.out.println("\n--- Materials on site created ---\n");
											conn.commit();
										} else {
											System.out.println(
													"\nMaterials on site couldnt be created, nothing pushed to database\n");
											conn.rollback(save);
										}
									} else {
										System.out.println("\nMaterials couldnt be created");
										conn.rollback(save);
									}

								} else {
									System.out.println("\nJob couldnt be created");
									conn.rollback(save);
								}

							} else {
								System.out.println("\nAddress couldnt be created");
								conn.rollback(save);
							}

						} else {
							System.out.println("\nClient couldnt be created");
						}

					} catch (SQLException ex) {
						// handle any errors
						Managment.sqlExceptionError(ex);
					}
				} else {
					System.out.println("\nMake sure the client doesnt already exist\n");
					createClient();
				}
			}

		}
	}

	private Materials createMaterials(String jobRef, String type) {
		Materials materials = null;
		int blocks = 0, bricks = 0, rafterAmount = 0, roofCover = 0, doors = 0, windows = 0, hinges = 0, handles = 0,
				locks = 0;
		float concrete = 0, studs = 0, plasterboard = 0, rafterLength = 0, skirtingboard = 0, archatrave = 0,
				guttering = 0;
		if (type.equalsIgnoreCase("required")) {
			boolean check = false;
			while (!check) {
				try {
					System.out.println(
							"\nPlease enter the following quantities/sizes needed for the job\nBlocks required:");
					blocks = scan.nextInt();
					System.out.println("Bricks:");
					bricks = scan.nextInt();
					System.out.println("Concrete (Cubic meters):");
					concrete = scan.nextFloat();
					System.out.println("Studding timber (meters):");
					studs = scan.nextFloat();
					System.out.println("Plasterboard (meters):");
					plasterboard = scan.nextFloat();
					System.out.println("Rafters amount:");
					rafterAmount = scan.nextInt();
					System.out.println("Rafter length (meters):");
					rafterLength = scan.nextFloat();
					System.out.println("Roof cover:");
					roofCover = scan.nextInt();
					System.out.println("Skirtingboard (meters):");
					skirtingboard = scan.nextFloat();
					System.out.println("archatrave (meters):");
					archatrave = scan.nextFloat();
					System.out.println("Guttering (meters):");
					guttering = scan.nextFloat();
					System.out.println("Doors:");
					doors = scan.nextInt();
					System.out.println("Windows:");
					windows = scan.nextInt();
					System.out.println("Hinges:");
					hinges = scan.nextInt();
					System.out.println("Handles:");
					handles = scan.nextInt();
					System.out.println("Locks");
					locks = scan.nextInt();

				} catch (InputMismatchException e) {
					System.out.println("\nMake sure you enter only numbers");
					scan.nextLine();
				}

				if (blocks > 0 && bricks > 0 && concrete > 0 && studs > 0 && plasterboard > 0 && rafterAmount > 0
						&& rafterLength > 0 && roofCover > 0 && skirtingboard > 0 && archatrave > 0 && guttering > 0
						&& doors > 0 && windows > 0 && hinges > 0 && handles > 0 && locks > 0) {
					check = true;

					materials = new Materials(null, jobRef, blocks, bricks, concrete, studs, plasterboard, rafterAmount,
							rafterLength, roofCover, skirtingboard, archatrave, guttering, doors, windows, hinges,
							handles, locks, "Required");
				}
			}

		} else if (type.equalsIgnoreCase("onsite")) {
			materials = new Materials(null, jobRef, blocks, bricks, concrete, studs, plasterboard, rafterAmount,
					rafterLength, roofCover, skirtingboard, archatrave, guttering, doors, windows, hinges, handles,
					locks, "OnSite");
		}

		return materials;
	}

	public Employee viewWorker() {

		Managment.allWorkers();
		boolean flag = false;
		Employee worker = null;
		while (!flag) {
			System.out.println("\nEnter the workers first name");
			String firstName = scan.nextLine();
			System.out.println("Enter the workers second name");
			String secondName = scan.nextLine();

			try {
				if (this.isAdmin()) {
					for (Employee w : Managment.workers) {
						if (w.getFirstName().equalsIgnoreCase(firstName)
								&& w.getSecondName().equalsIgnoreCase(secondName)) {
							flag = true;
							worker = new Employee(w.getFirstName(), w.getSecondName(), w.getId(), w.getJobRef(),
									w.getWage(), w.getOccupation());
							System.out.println("\n----\nWorkerID: " + worker.getId() + "\nFirst name: "
									+ worker.getFirstName() + "\nSecond name: " + worker.getSecondName()
									+ "\nOccupation: " + worker.getOccupation());

							if (!worker.getOccupation().equalsIgnoreCase("Admin")) {
								System.out.println("Wage: �" + worker.getWage());
							}
							System.out.println("Job Reference: " + worker.getJobRef() + "\n----");
						}
					}
				} else {
					for (Employee w : Managment.workersOnSite) {
						if (w.getFirstName().equalsIgnoreCase(firstName)
								&& w.getSecondName().equalsIgnoreCase(secondName)) {
							flag = true;
							worker = new Employee(w.getFirstName(), w.getSecondName(), w.getId(), w.getJobRef(),
									w.getWage(), w.getOccupation());
						}
					}
					System.out.println("\n----\nWorkerID: " + worker.getId() + "\nFirst name: " + worker.getFirstName()
							+ "\nSecond name: " + worker.getSecondName() + "\nOccupation: " + worker.getOccupation());

					if (!worker.getOccupation().equalsIgnoreCase("Admin")
							&& !worker.getOccupation().equalsIgnoreCase("SiteManager")) {
						System.out.println("Wage: �" + worker.getWage());
					}
					System.out.println("Job Reference: " + worker.getJobRef() + "\n----");
				}
			} catch (NullPointerException e) {
				// System.out.println("\nWorker doesnt exist (May not be on your site)");
				System.out.println(e);
			}
			if (worker == null)
				System.out.println("\nSorry this worker doesnt exist, try again");
		}
		return worker;
	}

	public void editWorker() {

		// make the connection to the database using connect class
		Connect c = new Connect();
		Connection conn = c.makeConnnection();

		Employee worker = viewWorker();
		boolean flag = false;
		String choice = "";
		while (!flag) {
			System.out.println(
					"What would you like to edit?\n 1. Occupation \n 2. First name \n 3. Second name \n 4. Wage\n");
			choice = scan.nextLine();

			if (choice.matches("[1-4]|[bB]")) {
				flag = true;
				switch (choice) {
				case "1":
					choice = "Occupation";
					break;
				case "2":
					choice = "FirstName";
					break;
				case "3":
					choice = "SecondName";
					break;
				case "4":
					choice = "Wage";
					break;
				case "B":
				case "b":
					return;
				}
			} else {
				System.out.println("\nPlease Input one of the options\n");
			}
		}

		boolean check = false;
		while (!check) {
			System.out.println("What would you like to change " + choice + " to?");
			String change = scan.nextLine();

			if (!choice.equals("Wage") && change.matches("[a-zA-Z]+")) {
				check = true;
			} else if (choice.equals("Wage") && change.matches("\\d+.?\\d*")) {
				check = true;
			} else {
				System.out.println("\nPlease use numbers for wage and letters for other\n");
			}
			if (check) {
				String sql = "update workers set " + choice + "=" + '"' + change + '"' + " where workerid=?;";

				try {

					PreparedStatement stmt = conn.prepareStatement(sql);
					stmt.setString(1, worker.getId());

					int count = stmt.executeUpdate();
					if (count > 0) {
						System.out.println("\nWorker updated\n");
					} else {
						System.out.println("\nWorker couldnt be updated\n");
					}
				} catch (SQLException ex) {
					// handle any errors
					Managment.sqlExceptionError(ex);
				}
			}
		}
	}

	public void createWorker() {
		// make the connection to the database using connect class
		Connect c = new Connect();
		Connection conn = c.makeConnnection();

		boolean check = false;
		while (!check) {
			String sql = "insert into Workers(Occupation, FirstName, SecondName, Wage, JobID)values(?, ?, ?,?,?);";
			String jobRef = "";
			System.out.println("---- Please enter the following credentials of the worker ----\nFirst name: ");
			String firstName = scan.nextLine();
			System.out.println("Second name: ");
			String secondName = scan.nextLine();
			System.out.println("Occupation: ");
			String occupation = scan.nextLine();
			System.out.println("Wage: ");
			String wage = scan.nextLine();

			if (firstName.matches("[a-zA-Z]+") && secondName.matches("[a-zA-Z]+")
					&& occupation.matches("[a-zA-Z]+( ?[a-zA-Z]*)") && wage.matches("[0-9]+.?[0-9]+")) {
				check = true;
				String firstNameUpper = firstName.substring(0, 1).toUpperCase() + firstName.substring(1).toLowerCase();
				String secondNameUpper = secondName.substring(0, 1).toUpperCase()
						+ secondName.substring(1).toLowerCase();
				String occupationUpper = occupation.substring(0, 1).toUpperCase()
						+ occupation.substring(1).toLowerCase();

				System.out.println("Would you like to assign this worker to a job now? (Y/N)");
				String assignJobRef = scan.nextLine();

				if (assignJobRef.equalsIgnoreCase("n") || assignJobRef.equalsIgnoreCase("no")) {
					jobRef = null;
				} else if (assignJobRef.equalsIgnoreCase("y") || assignJobRef.equalsIgnoreCase("yes")) {
					boolean check2 = false;

					while (check2 == false) {
						System.out.println("\nPlease enter the job reference you want to assign the worker to");
						jobRef = scan.nextLine();

						if (!Managment.isExists("Select jobid from jobs where jobid =?", jobRef)) {
							System.out.println("\nSorry that job reference doesnt exist");
						} else {
							check2 = true;
						}
					}
				}
				try {

					PreparedStatement stmt = conn.prepareStatement(sql);
					stmt.setString(1, occupationUpper);
					stmt.setString(2, firstNameUpper);
					stmt.setString(3, secondNameUpper);
					stmt.setString(4, wage);
					stmt.setString(5, jobRef);

					int count = stmt.executeUpdate();
					if (count > 0) {
						System.out.println("\nWorker created\n");
					} else {
						System.out.println("\nWorker couldnt be created");
					}

					if (occupation.equalsIgnoreCase("admin") || occupation.equalsIgnoreCase("site manager")) {

						System.out.println("Please enter the login credentials of this user\nUsername: ");
						String username = scan.nextLine();
						System.out.println("Password: ");
						String password = scan.nextLine();

						// hash the password using md5
						String hashPass = Managment.hashPass(password);

						sql = "insert into users(WorkerID, Username, Password)values((select LAST_INSERT_ID()), ?, ?);";
						stmt = conn.prepareStatement(sql);
						stmt.setString(1, username);
						stmt.setString(2, hashPass);

						count = stmt.executeUpdate();

						if (count > 0) {
							System.out.println("\nUser created");
							Managment.allWorkers();
						} else {
							System.out.println("\nUser couldnt be created");
						}
					}
				} catch (SQLException ex) {
					// handle any errors
					Managment.sqlExceptionError(ex);
				}
				System.out.println("\nWould you like to create another worker? (Y/N)");
				String choice = scan.nextLine();
				if (choice.equalsIgnoreCase("Y") || choice.equalsIgnoreCase("yes")) {
					createWorker();
				} else {
					return;
				}
			} else {
				System.out.println("\nPlease check what you have entered is valid/not empty\n");
			}
		}

	}

	public void moveWorker() {
		// make the connection to the database using connect class
		Connect c = new Connect();
		Connection conn = c.makeConnnection();

		Employee worker = viewWorker();

		boolean check = false;

		while (check == false) {
			System.out.println("\nPlease enter the job reference you want to assign the worker to");
			String jobRef = scan.nextLine();

			if (!Managment.isExists("Select jobid from jobs where jobid =?", jobRef)) {
				System.out.println("\nSorry that job reference doesnt exist");
			} else {
				check = true;

				if (firstName.equalsIgnoreCase(this.getFirstName())
						&& secondName.equalsIgnoreCase(this.getSecondName())) {
					this.setJobRef(jobRef);
					Managment.getJob(jobRef);
				}
				String sql = "update workers set jobid=? where workerid=?";
				try {
					PreparedStatement stmt = conn.prepareStatement(sql);
					stmt.setString(1, jobRef);
					stmt.setString(2, worker.getId());

					int count = stmt.executeUpdate();

					if (count > 0) {
						System.out.println("\nWorker has been moved\n");
						Managment.allWorkers();
					} else {
						System.out.println("\nWorker couldnt be moved\n");
					}
				} catch (SQLException ex) {
					// handle any errors
					Managment.sqlExceptionError(ex);
				}
				System.out.println("\nWould you like to move another worker? (Y/N)");
				String choice = scan.nextLine();

				if (choice.equalsIgnoreCase("Y") || choice.equalsIgnoreCase("yes")) {
					moveWorker();
				} else if (choice.equalsIgnoreCase("n") || choice.equalsIgnoreCase("no")) {
					return;
				} else {
					return;
				}
			}
		}
	}

	public void deleteWorker() {

		// make the connection to the database using connect class
		Connect c = new Connect();
		Connection conn = c.makeConnnection();
		Managment.allWorkers();
		boolean check = false;
		while (!check) {
			System.out.println("\nPlease renter your credentials for verification\nUsername: ");
			String username = scan.nextLine();
			System.out.println("Password: ");
			String password = scan.nextLine();

			// hash the password using md5
			String hashPass = Managment.hashPass(password);

			if (username.equalsIgnoreCase(this.getUsername()) && hashPass.equals(this.getPassword())) {
				check = true;
				boolean flag = false;
				boolean exists = false;
				while (flag != true) {
					System.out.println("Please enter the ID of the worker you want to delete (can not be your own ID)");
					String id = scan.nextLine();

					String sql = "delete from workers where workerid=?";

					// check that the worker exists and that they aren't the same worker as the one
					// logged in
					for (Employee w : Managment.workers) {
						if (w.getId().equals(id) && !id.equals(this.getId())) {
							exists = true;
							System.out.println("\n----\nWorkerID: " + w.getId() + "\nFirst name: " + w.getFirstName()
									+ "\nSecond name: " + w.getSecondName() + "\nOccupation: " + w.getOccupation()
									+ "\nWage: �" + w.getWage() + "\nJob Reference: " + w.getJobRef() + "\n----");
							System.out.println("\nIs this the worker you want to delete?(Y/N)");
							String choice = scan.nextLine();
							if (choice.equalsIgnoreCase("Y") || choice.equalsIgnoreCase("yes")) {
								flag = true;
							}
						}
					}
					if (!exists) {
						System.out.println("\nSorry this worker id doesnt exist\n");
					}
					if (flag == true) {
						try {
							PreparedStatement stmt = conn.prepareStatement(sql);
							stmt.setString(1, id);
							int count = stmt.executeUpdate();

							if (count > 0) {
								System.out.println("\nWorker has been deleted\n");
							} else {
								System.out.println("\nWorker could not be deleted\n");
							}
						} catch (SQLException ex) {
							// handle any errors
							Managment.sqlExceptionError(ex);
						}
					}
				}
			} else {
				System.out.println("\nSome of the credentials you entered were incorrect please retry");
			}
		}
		System.out.println("\nWould you like to delete another worker? (Y/N)");
		String choice = scan.nextLine();
		if (choice.equalsIgnoreCase("Y") || choice.equalsIgnoreCase("yes")) {
			deleteWorker();
		} else {

			return;
		}

	}

	public void progressReport() {

		if (this.getJobRef() == null || this.getJobRef().isEmpty()) {
			System.out.println("\nYou are not assigned to a job\n");
			return;
		}
		// make the connection to the database using connect class
		Connect c = new Connect();
		Connection conn = c.makeConnnection();

		LocalDate d = LocalDate.now();
		String date = d.toString();
		boolean check = false;
		while (!check) {

			System.out.println("\nPlease enter the following information\nProgress made: ");
			String progressMade = scan.nextLine();
			System.out.println("\nNext progress target: ");
			String progressTarget = scan.nextLine();
			if (!progressMade.isEmpty() && !progressTarget.isEmpty()) {
				check = true;

				String sql = "insert into progressreports(TheDate, ProgressMade, ProgressNextTarget, JobID)values(?, ?, ?, ?);";
				try {

					PreparedStatement stmt = conn.prepareStatement(sql);
					stmt.setString(1, date);
					stmt.setString(2, progressMade);
					stmt.setString(3, progressTarget);
					stmt.setString(4, this.getJobRef());

					int count = stmt.executeUpdate();

					if (count > 0) {
						System.out.println("\nProgress report made\n");
					} else {
						System.out.println("\nProgress report could not be made\n");
					}
				} catch (SQLException ex) {
					// handle any errors
					Managment.sqlExceptionError(ex);
				}
			} else {
				System.out.println("Dont leave any fields blank");
			}

		}
	}

	public void updateMaterials() {
		// make the connection to the database using connect class
		if (this.getJobRef() != null) {
			Connect c = new Connect();
			Connection conn = c.makeConnnection();

			boolean flag = false;
			String type = "";
			String choice = "";
			int num;
			while (!flag) {
				System.out.println(
						"What would you like to edit?\n 1. Number of blocks \n 2. Number of bricks \n 3. Cubic meters of concrete \n 4. Meters of studs \n 5. Square meters of plasterboard \n 6. Number of rafters \n 7. Meters of rafters \n 8. Meters of skirting board \n 9. Meters of archatrave \n 10. Meters of guttering \n 11. Number of doors \n 12. Number of windows \n 13. Number of hinges \n 14. Number of handles \n 15. Number of locks \n 16. Amount of roof cover\n");
				choice = scan.nextLine();
				num = Integer.parseInt(choice);
				if (choice.matches("\\d{1,2}|[bB]") && num <= 16) {
					flag = true;
					switch (choice) {
					case "1":
						choice = "Blocks";
						type = "number";
						break;
					case "2":
						choice = "Bricks";
						type = "number";
						break;
					case "3":
						choice = "Concrete";
						type = "float";
						break;
					case "4":
						choice = "StuddingTimber";
						type = "float";
						break;
					case "5":
						choice = "PlasterBoard";
						type = "float";
						break;
					case "6":
						choice = "RafterAmount";
						type = "number";
						break;
					case "7":
						choice = "RafterLength";
						type = "float";
						break;
					case "8":
						choice = "SkirtingBoard";
						type = "float";
						break;
					case "9":
						choice = "Archatrave";
						type = "float";
						break;
					case "10":
						choice = "Guttering";
						type = "float";
						break;
					case "11":
						choice = "Doors";
						type = "number";
						break;
					case "12":
						choice = "Windows";
						type = "number";
						break;
					case "13":
						choice = "Hinges";
						type = "number";
						break;
					case "14":
						choice = "Handles";
						type = "number";
						break;
					case "15":
						choice = "Locks";
						type = "number";
						break;
					case "16":
						choice = "RoofCover";
						type = "number";
						break;
					case "B":
					case "b":
						return;
					}
				} else {
					System.out.println("\nPlease Input one of the options\n");
				}

			}

			boolean check = false;
			String change = "";

			while (!check) {

				System.out.println("What would you like to change " + choice + " to?");
				change = scan.nextLine();
				float changeFloat = 0;
				int changeInt = 0;
				Class[] s = new Class[1];

				if (type.equals("number") && change.matches("\\d+")) {
					check = true;
					changeInt = Integer.parseInt(change);
					// reflection, get the method name dynamically and set the value to user input
					s[0] = int.class;
					try {
						Managment.onSiteMaterials.getClass().getMethod("set" + choice, s)
								.invoke(Managment.onSiteMaterials, changeInt);
					} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
							| NoSuchMethodException | SecurityException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else if (type.equals("float") && change.matches("\\d+.?\\d*")) {
					check = true;
					changeFloat = Float.parseFloat(change);
					// reflection, get the method name dynamically and set the value to user input
					s[0] = float.class;
					try {
						Managment.onSiteMaterials.getClass().getMethod("set" + choice, s)
								.invoke(Managment.onSiteMaterials, changeFloat);
					} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
							| NoSuchMethodException | SecurityException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else {
					System.out
							.println("\nPlease use correct formats for selection (Numbers/Decimal points if needed)\n");
				}
			}
			Managment.materialsList.replace("OnSite", Managment.onSiteMaterials);

			String sql = "update materials set " + choice + "=" + '"' + change + '"' + " where jobId=? and Type = "
					+ '"' + "OnSite" + '"' + ";";

			try {
				PreparedStatement stmt = conn.prepareStatement(sql);
				stmt.setString(1, this.getJobRef());

				int count = stmt.executeUpdate();
				if (count > 0) {
					System.out.println("\nMaterials updated\n");
				} else {
					System.out.println("\nmaterials couldnt be updated\n");
				}
			} catch (SQLException ex) {
				// handle any errors
				Managment.sqlExceptionError(ex);
			}
		} else {
			System.out.println("\nYou are not assigned to a job\n");
		}
	}

	public void deleteJob() {
		// make the connection to the database using connect class
		Connect c = new Connect();
		Connection conn = c.makeConnnection();
		boolean check = false;
		while (!check) {
			System.out.println("\nPlease reenter your credentials for verification\nUsername: ");
			String username = scan.nextLine();
			System.out.println("Password: ");
			String password = scan.nextLine();

			// hash the password using md5
			String hashPass = Managment.hashPass(password);

			if (username.equalsIgnoreCase(this.getUsername()) && hashPass.equals(this.getPassword())) {
				check = true;
				boolean flag = false;
				while (!flag) {
					System.out.println("Please enter the job reference of the job you want to delete");
					String id = scan.nextLine();

					String sql = "delete from jobs where JobID=?";

					if (Managment.isExists("Select jobid from jobs where jobid =?", id)) {
						flag = true;
						try {
							PreparedStatement stmt = conn.prepareStatement(sql);
							stmt.setString(1, id);
							int count = stmt.executeUpdate();

							if (count > 0) {
								System.out.println("\nJob has been deleted\n");
							} else {
								System.out.println("\nJob could not be deleted\n");
							}
						} catch (SQLException ex) {
							// handle any errors
							Managment.sqlExceptionError(ex);
						}
					} else {
						System.out.println("Sorry this job doesnt exist");
					}

				}
			} else {
				System.out.println("\nSome of the credentials you entered were incorrect please retry");
			}
		}
	}

}
