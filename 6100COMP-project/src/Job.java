import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

public class Job {

	private Date date;
	private double price;
	private String jobRef;
	private Map <String, Materials> materials;
	private ArrayList <Employee> workersOnSite;
	private String addressId;
	 
	Connection conn = new Connect().makeConnnection();
	
	
	public Job(Date date, double price, String jobRef, Map<String, Materials> materials,
			ArrayList <Employee> workersOnSite, String addressId) {
		super();
		this.date = date;
		this.price = price;
		this.jobRef = jobRef;
		this.materials = materials;
		this.workersOnSite = workersOnSite;
		this.addressId = addressId;
	}


	public Date getDate() {
		return date;
	}


	public void setDate(Date date) {
		this.date = date;
	}


	public double getPrice() {
		return price;
	}


	public void setPrice(double price) {
		this.price = price;
	}


	public String getJobRef() {
		return jobRef;
	}


	public void setJobRef(String jobRef) {
		this.jobRef = jobRef;
	}


	public Map<String, Materials> getMaterials() {
		return materials;
	}


	public void setMaterials(Map<String, Materials> materials) {
		this.materials = materials;
	}

	public ArrayList <Employee> getWorkersOnSite() {
		return workersOnSite;
	}


	public void setMenWorking(ArrayList <Employee> workersOnSite) {
		this.workersOnSite = workersOnSite;
	}
	
	
	
	public String getAddressId() {
		return addressId;
	}


	public void setAddressId(String addressId) {
		this.addressId = addressId;
	}


	public void materialsOnSite() {
		
		
		
	}
	
	public void viewJob() {
				
		System.out.println("----\nJob Reference: "+ this.getJobRef()+ "\nDeadline: " + this.getDate()+"\n\nclient - \nName: " + Managment.client.getFirstName() + " " + Managment.client.getSecondName()
		+ "\nPhone number: " + Managment.client.getPhoneNumber() + "\nEmail address: "
		+ Managment.client.getEmailAddress());
		
		getAddress();

	}

	public void getAddress() {
		try {
			String sql = "Select * from addresses where addressID=?";
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setString(1, this.getAddressId());

			ResultSet rs = stmt.executeQuery();

			if (!rs.isBeforeFirst()) {
				System.out.println("\nAddress doesnt exist");
			} else {
				while (rs.next()) {
					System.out.println("\nAddress - \nHouse number: " + rs.getString("HouseNumber") + "\nLine one: "
							+ rs.getString("LineOne") + "\nLine two: " + rs.getString("LineTwo") + "\nPostcode: "
							+ rs.getString("Postcode") + "\nCounty: " + rs.getString("County") + "\nCity: "
							+ rs.getString("City") + "\n----");

				}
			}
		} catch (SQLException ex) {
			// handle any errors
			Managment.sqlExceptionError(ex);
		}
	}
	
	public void printMaterials(String type) {

		//loop ever entry in the map then check if it has type required if so print
		for (Map.Entry<String, Materials> mat : this.materials.entrySet()) {
			if (type.equalsIgnoreCase("required")) {
				if (mat.getValue().getType().equalsIgnoreCase("Required"))
					System.out.println("--- Required materials for job " + mat.getValue().getJobRef()
							+ " ---\n\nBlocks: " + mat.getValue().getBlocks() + "\nBricks: "
							+ mat.getValue().getBricks() + "\nConcrete: " + mat.getValue().getConcrete()
							+ " cubic meters" + "\nStudding timber: " + mat.getValue().getStuddingTimber() + " meters"
							+ "\nPlasterboard: " + mat.getValue().getPlasterBoard() + " squared meters" + "\nRafters: "
							+ mat.getValue().getRafterAmount() + "     Rafter length: "
							+ mat.getValue().getRafterLength() + " meters" + "\nRoof cover: "
							+ mat.getValue().getRoofCover() + "\nSkirting boards: " + mat.getValue().getSkirtingBoard()
							+ " meters" + "\nArchatrave: " + mat.getValue().getArchatrave() + " meters"
							+ "\nGuttering: " + mat.getValue().getGuttering() + " meters" + "\nDoors: "
							+ mat.getValue().getDoors() + "\nWindows: " + mat.getValue().getWindows() + "\nHinges: "
							+ mat.getValue().getHinges() + "\nHandles: " + mat.getValue().getHandles() + "\nLocks: "
							+ mat.getValue().getLocks() + "\n");
				
				//check if it has type onSite if so print
			} else if (type.equalsIgnoreCase("onSite")) {
				if (mat.getValue().getType().equalsIgnoreCase("onSite"))
					System.out.println("--- Materials on site " + mat.getValue().getJobRef() + " ---\n\nBlocks: "
							+ mat.getValue().getBlocks() + "\nBricks: " + mat.getValue().getBricks() + "\nConcrete: "
							+ mat.getValue().getConcrete() + " cubic meters" + "\nStudding timber: "
							+ mat.getValue().getStuddingTimber() + " meters" + "\nPlasterboard: "
							+ mat.getValue().getPlasterBoard() + " squared meters" + "\nRafters: "
							+ mat.getValue().getRafterAmount() + "     Rafter length: "
							+ mat.getValue().getRafterLength() + " meters" + "\nRoof cover: "
							+ mat.getValue().getRoofCover() + "\nSkirting boards: " + mat.getValue().getSkirtingBoard()
							+ " meters" + "\nArchatrave: " + mat.getValue().getArchatrave() + " meters"
							+ "\nGuttering: " + mat.getValue().getGuttering() + " meters" + "\nDoors: "
							+ mat.getValue().getDoors() + "\nWindows: " + mat.getValue().getWindows() + "\nHinges: "
							+ mat.getValue().getHinges() + "\nHandles: " + mat.getValue().getHandles() + "\nLocks: "
							+ mat.getValue().getLocks() + "\n");
			}
		}

	}

}
